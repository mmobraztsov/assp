﻿-- Предподготовка
-- TRUNCATE vehicle_visit3;

-- INSERT INTO vehicle_visit3 (VIN, DealerCode, DealerName, OrderNumber, OrderType, OrderDate, InvoiceNumber, TransactionDate, 
-- TransactionMileage, TransactionVehicleAge)
-- SELECT 
--        VIN, DealerCode, DealerName, OrderNumber, OrderType, OrderDate, InvoiceNumber, TransactionDate, 
-- TransactionMileage, TransactionVehicleAge FROM vehicle_visit ORDER BY TransactionDate ASC;

-- Наш поиск
SET @start_time="2022-01-01";
SET @b_id1 = '4'; -- BMW
SET @b_id2 = '25'; -- MINI
SET @v_type = '1';-- автотранспорт
SET @DEALERlist = (SELECT GROUP_CONCAT(DISTINCT cd.company_id) FROM clc_dealer cd 
 WHERE cd.brand_id IN (@b_id1,@b_id2) AND cd.vehicle_type IN (@v_type)); 
SELECT
 cd.bmw_id AS 'BMW_id',
 cd.company_id AS 'CO_id',
 cd.dealer_name AS 'DC_name',
 crlp.vehicle_vin,
 IF(vv.id IS NULL ,0, 
 IF( vv.DealerCode=cd.bmw_id, 1, 2)) AS 'заехал ли',
 vv.DealerCode AS 'куда заехал'

 FROM clc_ring_log crl 
 join clc_ring_list_person crlp ON crl.ring_list_person_id = crlp.id  AND  FIND_IN_SET(crlp.company_id, @dealerlist) > 0
 join clc_ring_list crl1 ON crlp.ring_list_id = crl1.id
 join clc_dealer cd ON crlp.company_id = cd.company_id AND crl1.brand_id = cd.brand_id AND crl1.vehicle_type = cd.vehicle_type
 left JOIN assp.vehicle_visit vv ON vv.VIN=crlp.vehicle_vin AND TIMESTAMPDIFF(day, crl.start_time, vv.TransactionDate) > 60 AND 
 vv.id = 
 (SELECT MIN(vv1.id) FROM assp.vehicle_visit vv1 WHERE vv1.VIN=crlp.vehicle_vin 
 AND TIMESTAMPDIFF(day, crl.start_time, vv1.TransactionDate) > 60)

JOIN clc_ring_log_decline crld ON crl.id = crld.ring_log_id  AND crld.reject_reason_id=14
join clc_reject_reason crr ON crld.reject_reason_id = crr.id 
WHERE date(crl.start_time) > @start_time