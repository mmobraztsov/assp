﻿#
#
# ЗАПРОС ВЫВОД ДОПОВ ЗА ПЕРИОД
#
# выбор периода
SET @start_time='2022-01-01';
SET @end_time='2022-12-31 23:59:59';
#
# генерация списка ДЦ по бренду и типу т/с
# выбор бренда и т/с
 SET @b_id1 = '4';-- BMW
 SET @b_id2 = '25';-- MINI
-- SET @b_id1 = '29'; --Peugot
-- SET @b_id2 = '8'; -- Citroen
-- SET @b_id1 = '75'; -- unnamed eurorepar
-- SET @b_id2 = '75'; -- unnamed eurorepar
-- SET @b_id1 = '66'; -- газ
-- SET @b_id2 = '66'; -- газ
SET @v_type = '1';-- автотранспорт
#
 SET @company_id = (SELECT GROUP_CONCAT(DISTINCT cd.company_id) FROM clc_dealer cd WHERE cd.brand_id IN (@b_id1,@b_id2) AND cd.vehicle_type IN (@v_type)); 
# set @dealerlist="2884";
#SET @company_id = (SELECT GROUP_CONCAT(DISTINCT `ecm3-all`.clc_dealer.company_id) FROM `ecm3-all`.clc_dealer WHERE `ecm3-all`.clc_dealer.brand_id IN (4,25) AND `ecm3-all`.clc_dealer.vehicle_type IN (1));
#
# выбор ринг листов
SET @rlist = (SELECT GROUP_CONCAT(DISTINCT `ecm3-all`.clc_ring_list.id) FROM `ecm3-all`.clc_ring_list
                                WHERE `ecm3-all`.clc_ring_list.brand_id IN (@b_id1,@b_id2) AND `ecm3-all`.clc_ring_list.vehicle_type IN (@v_type)
                                AND `ecm3-all`.clc_ring_list.start_date >= @start_time
                                AND `ecm3-all`.clc_ring_list.start_date <=@end_time);

SELECT 
  ccs.bmw_id AS BMW_id,
  ccs.company_id AS CO_id,
  ccs.dealer_name AS DC_name,
  dopct.descr AS Dop_cat,
 -- dopct.id,
 cd.descr as dop_descr,
  (COUNT(cdr.dop_id) ) AS rating,
  (IFNULL(cdl.create_time_start, 'Не логировалось') ) as dop_start_time,
  (IFNULL(cdl2.create_time_start, 'Активен') ) AS dop_end_time,
  ma.descr AS 'Region_name', ma.code AS 'Region_code'

  FROM clc_dop_rating cdr 
  JOIN clc_dop cd ON cd.id=cdr.dop_id 
  JOIN clc_ring_list_person crlp ON crlp.id=cdr.ring_list_person_id 
--  JOIN clc_ring_list crl ON crl.id=crlp.ring_list_id
  JOIN clc_ring_list crl ON crl.id=crlp.ring_list_id and FIND_IN_SET(crl.id, @rlist) 
  JOIN clc_dealer ccs ON ccs.company_id=crlp.company_id AND ccs.vehicle_type=crl.vehicle_type AND ccs.brand_id= crl.brand_id AND FIND_IN_SET(ccs.company_id,@company_id) 
  JOIN rse_company rc ON cd.company_id = rc.id
  JOIN mst_city mc ON rc.city_id = mc.id
  JOIN mst_area ma ON mc.area_id = ma.id
  JOIN clc_dop_category dopct ON cd.dop_category_id = dopct.id
  LEFT JOIN clc_dop_log cdl ON cd.id = cdl.dop_id AND cdl.param_field='is_active' AND cdl.param_value=1 AND cdl.id=
  (SELECT MIN(cdl_1.id) FROM clc_dop_log cdl_1 where cdl.dop_id = cdl_1.dop_id AND cdl_1.param_field='is_active' AND cdl_1.param_value=1) 
  LEFT JOIN clc_dop_log cdl2 ON cd.id = cdl2.dop_id AND cdl2.param_field='is_active' AND cdl2.param_value=0 AND cdl2.id= 
  (SELECT max(cdl2_1.id) FROM clc_dop_log cdl2_1 where cdl2.dop_id = cdl2_1.dop_id AND cdl2_1.param_field='is_active')
  WHERE date(cdr.create_time) BETWEEN @start_time AND @end_time
  GROUP BY cdr.dop_id ORDER BY ccs.dealer_name ASC
  -- cdr.dop_id ASC