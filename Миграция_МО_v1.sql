﻿-- Миграция клиентов

-- Параметры:
SET @dealer_code = 29691;
-- ,35227 26959 32481;
SET @date1 = '2022-01-01';
SET @date2 = '2022-04-24';
set @hold = 1;  -- 1-для одного ДЦ
                -- 2-для холдинга

-- ============================================================

-- Возрастные категории и их нижние границы
DROP TEMPORARY TABLE IF EXISTS assp.tmp_age_cat;
CREATE TEMPORARY TABLE assp.tmp_age_cat (
  id smallint UNSIGNED NOT NULL PRIMARY KEY,
  age_from smallint UNIQUE,
  descr varchar(64)
);
INSERT assp.tmp_age_cat (id, age_from, descr) VALUES
(0, 0, '1…3'), # Для возраста 0, 1 или 2 года
(1, 3, '4…5'), # Для возраста 3 или 4 года
(2, 5, '5+')   # Для возраста 5 лет и старше
;

-- ============================================================

-- Определитель возрастных категорий по возрасту (от 0 до 99 лет)
DROP TEMPORARY TABLE IF EXISTS assp.tmp_age_def;
CREATE TEMPORARY TABLE assp.tmp_age_def (
  age smallint PRIMARY KEY,
  age_cat smallint UNSIGNED
);
INSERT assp.tmp_age_def (age, age_cat)
WITH RECURSIVE cte AS (
  SELECT 0 AS age
  UNION ALL
  SELECT age+1 FROM cte WHERE age < 99
)
SELECT age, MAX(tac.id) AS age_cat 
FROM cte
JOIN assp.tmp_age_cat tac ON cte.age >= tac.age_from
GROUP BY age
;

-- ============================================================

-- Направления миграции
DROP TEMPORARY TABLE IF EXISTS assp.tmp_migration_dir;
CREATE TEMPORARY TABLE assp.tmp_migration_dir (
  id int NOT NULL PRIMARY KEY,
  descr varchar(64)
);
INSERT assp.tmp_migration_dir (id, descr)  
SELECT bmw_id, name FROM topic.bmw_dealer_code UNION ALL
SELECT -5, 'Ранее не обслуживался' UNION ALL
SELECT -4, 'Москва'                UNION ALL
SELECT -3, 'Санкт-Петербург'       UNION ALL
SELECT -2, 'Другой регион'         UNION ALL
SELECT -1, 'Anonymous' 
;
UPDATE assp.tmp_migration_dir
SET descr = CONCAT('Без изменений (', descr, ')')
WHERE id = @dealer_code
;

-- ============================================================

-- Запрос 1. Список vin с датой посл. визита у выбранного дц по @date1
DROP TEMPORARY TABLE IF EXISTS assp.tmp_vv_d1;
CREATE TEMPORARY TABLE assp.tmp_vv_d1 (
  vin char(17) PRIMARY KEY, 
  old_dealer int UNSIGNED,
  old_age smallint,
  INDEX idxd (old_dealer)
);
INSERT assp.tmp_vv_d1 (vin, old_dealer, old_age)
SELECT 
  d.vin,
  d.dealer_code AS old_dealer,
  YEAR(@date1) - tabm.production_year AS old_age
FROM (
  SELECT 
    vv.VIN AS vin,
    vv.DealerCode AS dealer_code,
    ROW_NUMBER() OVER w AS rn
  FROM (
    SELECT DISTINCT VIN, DealerCode, TransactionDate
    FROM assp.vehicle_visit 
    WHERE TransactionDate <= @date1
  ) AS vv
  WINDOW w AS (PARTITION BY vv.VIN ORDER BY vv.TransactionDate DESC)
) AS d
JOIN assp.bmw_tabm tabm ON d.vin = tabm.vin
WHERE d.rn = 1
  AND tabm.ecode RLIKE '[EFGIR][0-9]{2}'
;

-- Запрос 2. Список vin с датой посл. визита у выбранного дц по @date2
DROP TEMPORARY TABLE IF EXISTS assp.tmp_vv_d2;
CREATE TEMPORARY TABLE assp.tmp_vv_d2 (
  vin char(17) PRIMARY KEY, 
  new_dealer int UNSIGNED,
  new_age smallint,
  INDEX idxd (new_dealer)
);
INSERT assp.tmp_vv_d2 (vin, new_dealer, new_age)
SELECT 
  d.vin,
  d.dealer_code AS old_dealer,
  YEAR(@date2) - tabm.production_year AS new_age
FROM (
  SELECT 
    vv.VIN AS vin,
    vv.DealerCode AS dealer_code,
    ROW_NUMBER() OVER w AS rn
  FROM (
    SELECT DISTINCT VIN, DealerCode, TransactionDate
    FROM assp.vehicle_visit 
    WHERE TransactionDate <= @date2
  ) AS vv
  WINDOW w AS (PARTITION BY vv.VIN ORDER BY vv.TransactionDate DESC)
) AS d
JOIN assp.bmw_tabm tabm ON d.vin = tabm.vin
WHERE d.rn = 1
;

-- ============================================================

-- Таблица миграций 
DROP TEMPORARY TABLE IF EXISTS assp.tmp_vv_migration;
CREATE TEMPORARY TABLE assp.tmp_vv_migration (
  vin char(17) PRIMARY KEY, 
  brand_id smallint UNSIGNED,

  old_dealer int UNSIGNED,
  old_holding int UNSIGNED,
  old_area_id smallint UNSIGNED,
  migration_to int,
  old_age smallint,
  old_age_cat smallint,

  new_dealer int UNSIGNED,
  new_holding int UNSIGNED,
  new_area_id smallint UNSIGNED,
  migration_from int,
  new_age smallint,
  new_age_cat smallint
);
INSERT assp.tmp_vv_migration (vin, brand_id, old_dealer, old_holding, old_area_id, old_age, migration_to, new_dealer, new_holding, new_area_id, new_age, migration_from)
SELECT 
  d2.vin,
  IF(tabm.brand = 'MINI', 25, 4) AS brand_id,

  d1.old_dealer,
  bdc1.bmw_holding_id,
  bdc1.area_id,
  d1.old_age,

  -- Кто куда уехал с начала периода (@date1)
  CASE 
    WHEN d2.new_dealer       = 0                   THEN -1             # Anonymous
    WHEN d1.old_dealer       = d2.new_dealer       THEN d2.new_dealer  # Сам дилер
    WHEN bdc1.bmw_holding_id = bdc2.bmw_holding_id THEN d2.new_dealer  # Другие ДЦ из холдинга
    WHEN bdc2.area_id        IN (10, 18)           THEN -4             # Москва
    WHEN bdc2.area_id        IN (72, 292)          THEN -3             # Санкт-Петербург
                                                   ELSE -2             # Другой регион
  END AS migration_to,

  d2.new_dealer,
  bdc2.bmw_holding_id,
  bdc2.area_id,
  d2.new_age,

  -- Кто откуда приехал на конец периода (@date2)
  CASE 
    WHEN IF(@hold=2, d1.old_dealer IS NULL, d1.old_dealer IS NULL
    AND tabm.last_sale_dealer_code NOT IN (SELECT bdc.bmw_id FROM topic.bmw_dealer_code bdc WHERE bdc.bmw_holding_id = (SELECT bdc.bmw_holding_id FROM topic.bmw_dealer_code bdc WHERE bdc.bmw_id=@dealer_code) AND bdc.bmw_id <> @dealer_code))
                                                   THEN -5             # Ранее не обслуживался
    WHEN d1.old_dealer       = 0                   THEN -1             # Anonymous
    WHEN d1.old_dealer       = d2.new_dealer       THEN d1.old_dealer  # Сам дилер
    WHEN bdc1.bmw_holding_id = bdc2.bmw_holding_id THEN d1.old_dealer  # Другие ДЦ из холдинга
    WHEN bdc1.area_id        IN (10, 18)           THEN -4             # Москва
    WHEN bdc1.area_id        IN (72, 292)          THEN -3             # Санкт-Петербург
                                                   ELSE -2             # Другой регион
  END AS migration_from

FROM assp.tmp_vv_d2 d2
JOIN assp.bmw_tabm tabm ON d2.vin = tabm.vin
JOIN topic.bmw_dealer_code bdc2 ON bdc2.bmw_id = d2.new_dealer
LEFT JOIN assp.tmp_vv_d1 d1 ON d1.vin = d2.vin
LEFT JOIN topic.bmw_dealer_code bdc1 ON bdc1.bmw_id = d1.old_dealer
-- LEFT JOIN topic.bmw_dealer_code bds ON bds.bmw_id = tabm.last_sale_dealer_code
;

-- Возрастные категории для начала периода (@date1)
UPDATE assp.tmp_vv_migration tvm, assp.tmp_age_def tad1 
SET tvm.old_age_cat = tad1.age_cat
WHERE IF(tvm.old_age < 0, 0, tvm.old_age) = tad1.age
;

-- Возрастные категории для конца периода (@date2)
UPDATE assp.tmp_vv_migration tvm, assp.tmp_age_def tad2 
SET tvm.new_age_cat = tad2.age_cat
WHERE IF(tvm.new_age < 0, 0, tvm.new_age) = tad2.age
;

-- ============================================================

-- Полная таблица
SELECT * FROM assp.tmp_vv_migration;

-- ============================================================

-- Откуда приехали к дилеру @dealer_code
SELECT 
  ANY_VALUE(tmd.descr) AS `Откуда приехали`,  
  ANY_VALUE(mb.descr) AS brand,
  SUM(m.new_age_cat=0) AS `1…3`,
  SUM(m.new_age_cat=1) AS `4…5`,
  SUM(m.new_age_cat=2) AS `5+`,
  COUNT(*) - COUNT(m.new_age_cat) AS `Возраст не известен`,
  COUNT(*) AS `Всего`

FROM assp.tmp_vv_migration m 
JOIN `ecm3-all`.mst_brand mb ON m.brand_id = mb.id
JOIN assp.tmp_migration_dir tmd ON tmd.id = m.migration_from
JOIN topic.bmw_dealer_code bdc ON bdc.bmw_id = m.new_dealer


WHERE m.new_dealer = @dealer_code
  AND IF(bdc.mini, TRUE, m.brand_id = 4)
--   AND m.migration_from <> @dealer_code

GROUP BY m.migration_from, m.brand_id 
ORDER BY m.migration_from, m.brand_id
;

-- Куда уехали от дилера @dealer_code
SELECT 
  ANY_VALUE(tmd.descr) AS `Куда уехали`,  
  ANY_VALUE(mb.descr) AS brand,
  SUM(m.old_age_cat=0) AS `1…3`,
  SUM(m.old_age_cat=1) AS `4…5`,
  SUM(m.old_age_cat=2) AS `5+`,
  COUNT(*) - COUNT(m.old_age_cat) AS `Возраст не известен`,
  COUNT(*) AS `Всего`

FROM assp.tmp_vv_migration m 
JOIN `ecm3-all`.mst_brand mb ON m.brand_id = mb.id
JOIN assp.tmp_migration_dir tmd ON tmd.id = m.migration_to
JOIN topic.bmw_dealer_code bdc ON bdc.bmw_id = m.old_dealer

WHERE m.old_dealer = @dealer_code
  AND IF(bdc.mini, TRUE, m.brand_id = 4)
--   AND m.migration_to <> @dealer_code

GROUP BY m.migration_to, m.brand_id 
ORDER BY m.migration_to, m.brand_id
;

-- Регион продажи ранее не обслуживавшихся и приехавших к дилеру @dealer_code
SELECT 
  ANY_VALUE(tmd.descr) AS `Регион продажи ранее не обслуживавшихся`,  
  ANY_VALUE(mb.descr) AS brand,
  SUM(m.new_age_cat=0) AS `1…3`,
  SUM(m.new_age_cat=1) AS `4…5`,
  SUM(m.new_age_cat=2) AS `5+`,
  COUNT(*) - COUNT(m.new_age_cat) AS `Возраст не известен`,
  COUNT(*) AS `Всего`
FROM (
  SELECT 
    m.brand_id,
    m.new_age_cat,
  
    -- Кто откуда приехал 
    CASE 
      WHEN bds.area_id IS NULL                       THEN -2                   # Anonymous
      WHEN tabm.last_sale_dealer_code = m.new_dealer THEN m.new_dealer         # Сам дилер
      WHEN bds.bmw_holding_id = m.new_holding        THEN tabm.last_sale_dealer_code  # Другие ДЦ из холдинга
      WHEN bds.area_id IN (10, 18)                   THEN -4                   # Москва
      WHEN bds.area_id IN (72, 292)                  THEN -3                   # Санкт-Петербург
                                                     ELSE -2                   # Другой регион
    END AS migration_from
  
  FROM assp.tmp_vv_migration m
  JOIN assp.bmw_tabm tabm ON m.vin = tabm.vin
  JOIN topic.bmw_dealer_code bdc ON bdc.bmw_id = m.new_dealer
  LEFT JOIN topic.bmw_dealer_code bds ON bds.bmw_id = tabm.last_sale_dealer_code

  WHERE m.new_dealer = @dealer_code
    AND IF(bdc.mini, TRUE, m.brand_id = 4)
    AND m.migration_from = -5
) AS m
JOIN `ecm3-all`.mst_brand mb ON m.brand_id = mb.id
JOIN assp.tmp_migration_dir tmd ON tmd.id = m.migration_from
GROUP BY m.migration_from, m.brand_id
ORDER BY m.migration_from, m.brand_id
;