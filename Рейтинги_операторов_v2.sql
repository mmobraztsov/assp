﻿use `ecm3-all`;

set @start_date = '2022-05-23';
set @end_date = '2022-05-23';

SELECT col.descr AS 'Оператор',
GROUP_CONCAT(DISTINCT(IF(col.location=1,'Дубна',IF(col.location=2,'НН','Аутсорсинг')))) AS 'Площадка', 
SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))) AS 'Длительность работы в скрипте',
IF(minute(SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))))>=30, hour(SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))))+1, hour(SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))))) AS 'Округл',
ROUND(COUNT(DISTINCT(ce.id))/IF(minute(SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))))>30, hour(SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))))+1, hour(SEC_TO_TIME(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time))))),2) AS 'Эффективность',
COUNT(DISTINCT(ce.id)) AS 'Заявки', 
COUNT(DISTINCT(crl.id)) AS 'Количество звонков', 
COUNT(DISTINCT(crl1.id)) AS 'Успешные',
COUNT(DISTINCT(crl.id)) - COUNT(DISTINCT(crl1.id)) AS 'Неуспешные',
ROUND((COUNT(DISTINCT(crl.id)) - COUNT(DISTINCT(crl1.id)))/COUNT(DISTINCT(crl.id))*100,1) AS 'Доля неуспешных',
SEC_TO_TIME((UNIX_TIMESTAMP(MAX(crl.end_time))-UNIX_TIMESTAMP(MIN(crl.start_time)))-(sum(UNIX_TIMESTAMP(crl.end_time) - unix_timestamp(crl.start_time)))) AS 'Cуммарное время простоя между карточками контактов',
COUNT(DISTINCT(crl3.id)) AS 'Кол-во карточек контактов с длительностью менее 20 сек',
SEC_TO_TIME(avg(UNIX_TIMESTAMP(crl1.end_time) - unix_timestamp(crl1.start_time))) AS 'Среднее время разговора с клиентом',
GROUP_CONCAT(DISTINCT cp.descr) AS 'Проект',
GROUP_CONCAT(DISTINCT crlp.ring_list_id) AS 'РЛ',
('') AS 'Комментарий'

FROM clc_ring_log crl 
left JOIN clc_ring_log crl1 ON crl.id = crl1.id and crl1.call_result_id in (6,9,10,11,12,15,16,17,18)
LEFT JOIN clc_ring_log crl3 ON crl.id = crl3.id AND (UNIX_TIMESTAMP(crl3.end_time) - unix_timestamp(crl3.start_time)) <= 20
JOIN clc_ring_list_person crlp ON crl.ring_list_person_id = crlp.id 
JOIN clc_operator_list col ON col.id=crl.user_id 
JOIN clc_ring_list crl2 ON crl2.id=crlp.ring_list_id
JOIN clc_project cp ON crl2.project_id = cp.id
left JOIN clc_dop_rating cdr on cdr.creator_id=crl.user_id and crlp.id=cdr.ring_list_person_id and date(cdr.create_time)=date(crl.start_time) 
-- left JOIN clc_contact cc ON crlp.person_id = cc.person_id AND cc.param_name='email2' AND cc.creator_id =crl.user_id AND date(cc.create_time)=date(crl.start_time) 
LEFT JOIN clc_event ce ON crlp.id = ce.ring_list_person_id and ce.event_goal_id= 2 AND ce.creator_id=col.id AND date(ce.create_time) BETWEEN @start_date AND @end_date and ce.status=1 and ce.event_bid_type_id <> 4
WHERE date(crl.start_time) BETWEEN @start_date AND @end_date
-- AND crl2.brand_id IN ('4') 
GROUP BY 1
ORDER BY 1 ASC


