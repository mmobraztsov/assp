﻿#
# Причина отказов по всем ДЦ за указанный период
#
# начальная дата
 SET @start_time="2022-01-01";
# генерация списка ДЦ по бренду и типу т/с
# выбор бренда и т/с
SET @b_id1 = '4'; -- BMW
SET @b_id2 = '25'; -- MINI
-- SET @b_id1 = '4';-- BMW
-- SET @b_id2 = '25';-- MINI
-- SET @b_id1 = '29'; --Peugot
-- SET @b_id2 = '8'; -- Citroen
-- SET @b_id1 = '75'; -- unnamed eurorepar
-- SET @b_id2 = '75'; -- unnamed eurorepar
-- SET @b_id1 = '66'; -- газ
-- SET @b_id2 = '66'; -- газ
SET @v_type = '1';-- автотранспорт
#
 SET @DEALERlist = (SELECT GROUP_CONCAT(DISTINCT cd.company_id) FROM clc_dealer cd WHERE cd.brand_id IN (@b_id1,@b_id2) AND cd.vehicle_type IN (@v_type)); 
# set @dealerlist="2884";
SELECT
 cd.bmw_id AS 'BMW_id',
 cd.company_id AS 'CO_id',
 cd.dealer_name AS 'DC_name',
 crld.reject_reason_id AS 'RR_id',
 crr.descr AS 'RR_descr',
 # блок для суммарного выода, удалить для вывода винов
  (COUNT(*)) AS 'Qnty' -- удалить для вывода винов
 # блок для вывода винов и даты окончания разговора
 -- crlp.vehicle_vin AS 'VIN',
 -- date(crl.end_time) AS 'Date'
 # конец блока

 FROM clc_ring_log crl 
 join clc_ring_list_person crlp ON crl.ring_list_person_id = crlp.id  AND  FIND_IN_SET(crlp.company_id, @dealerlist) > 0
 join clc_ring_list crl1 ON crlp.ring_list_id = crl1.id
 join clc_dealer cd ON crlp.company_id = cd.company_id AND crl1.brand_id = cd.brand_id AND crl1.vehicle_type = cd.vehicle_type

JOIN clc_ring_log_decline crld ON crl.id = crld.ring_log_id  AND crld.reject_reason_id BETWEEN 1 AND 20 #диапазон кодов отказов(!!!)
join clc_reject_reason crr ON crld.reject_reason_id = crr.id 
 WHERE date(crl.start_time) > @start_time
# блок для суммарного вывода, удалить для вывода винов
GROUP BY cd.id, crld.reject_reason_id -- удалить для вывода винов
