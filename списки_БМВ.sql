-- ������ ��� ���������� ������� ������� BMW/MINI
-- ====================================================================================


-- TODO:

-- ��� bigint unsigned ��� ����� clear_phone
-- ������ ������ ���� � ������� �������

-- ������ ���������

-- 2021-02-03 (build 04)
-- ������� �������� ������ ���������� ������ �� tmp_vin � ������� �������
-- ������ ������������ �������� ������������� (��������� � topic.ringlist_show_view)

-- 2021-01-21 (build 03)
-- ��������� ��������� �������� � ISOFIX

-- 2020-12-21 (build 02)
-- ��������� �������� 0000001401

-- 2020-10-29 (build 01)
-- ��������� �������������� � ������� �� 0 ��� ���������� mileage_predict
-- ����� contact_modified, email1, email2

-- 2020-10-20 (3.1)
-- ����� ���� � ������� �������: contact_modified_on, crm_id
-- contact_modified_on ������ contact_creation_date
-- ������� �� ���. � ����. ������ ������������: rf_min_idle, rf_max_idle

-- 2020-10-05
-- �������������� � ������� �� 0 ��� ���������� mileage_predict �� ���������

-- 2020-09-17 
-- ��������� ���� gender � ����� tmp_list

-- 
-- ����������� ��������. �� �������!!!
-- ====================================================================================
SET @RINGLIST_CREATOR_VERSION = '3.1';
SET @RINGLIST_CREATOR_BUILD = '04';

SET @BMW_BRAND   = 4;         # BMW
SET @MINI_BRAND  = 25;        # MINI

SET @AUTO_VEHICLE_TYPE = 1;   # ����������
SET @MOTO_VEHICLE_TYPE = 2;   # ��������

SET @STAGE = 30;              # ��������� �������� �������� �������� (���� ������ 30 ��������� � �������������� ������)

-- 
-- ������������ ���������
-- ====================================================================================
-- �������� ��������
SET @CAMPAIGN_NAME = '4-6-8 ������� ����';  

-- ���������� �� ������ � ���������� ������� (����� ������� ��� �������������)
SET @WRITE_PERMANENT = FALSE;           



-- 
-- ��������� ��� ������� � ���������������
-- ====================================================================================
SET @SILENCE_DAYS = 45;       # ������ ������ ��� �������, ���
SET @ACTIVITY_THRESHOLD = 10; # ������� ����������, ������
SET @MAX_IDLE_PERIOD = 60;    # ������������ ������ ������������, ������
SET @MIN_IDLE_DAYS = 45;      # ����������� ���������� ���� ����� ���������� ������
SET @ONLY_CYRILLIC = FALSE;   # ��������� ������� � ���� fullname

SET @BRAND_1 = @BMW_BRAND;
SET @BRAND_2 = @MINI_BRAND;
SET @VEHICLE_TYPE = @AUTO_VEHICLE_TYPE;


-- 
-- ������ ���������
-- ��������������, ���� �� �������� �������� �� ���������, ������� ������������� � ��������� ringlist_init
-- ====================================================================================
SET @LOG_TABLE      = NULL;  # 'assp.tmp_report';  # ��������� ������� ��� ������� (������)
SET @RAW_TABLE      = NULL;  # 'assp.tmp_vin';     # ��������� ������� ��� ��������� ������� �� ��������� ��������
SET @WORK_TABLE     = NULL;  # 'assp.tmp_p';       # ��������� ������� �������
SET @CAMPAIGN_TABLE = NULL;  # 'assp.wt_046';      # ���������� ������� ������� ��� ��������
SET @CRMID          = NULL;  # 1                   # crm_id, �� ��������� BMW (@BMW_BRAND)
SET @BASE_DATE      = topic.get_qlik_load_date();  # ������� ����, ����������� �� "�������". �� ��������� MAX(last_invoice_date) �� assp.bmw_tabm


-- 
-- 1. ������� �� ��������� ��������
-- ====================================================================================
-- ��� ������� ������ ���� �������� � @RAW_TABLE
DROP TEMPORARY TABLE IF EXISTS assp.tmp_vin;
CREATE TEMPORARY TABLE assp.tmp_vin (
  vin char(17) PRIMARY KEY,
  service_dealer varchar(255),
  service_dealer_code int UNSIGNED DEFAULT 0 NOT NULL,
  customer_group int,
  idle_period int,
  mileage_predict int
);


INSERT INTO assp.tmp_vin (vin, service_dealer, service_dealer_code, customer_group, idle_period, mileage_predict)

SELECT 
  tabm.vin,
  tabm.service_dealer,
  IFNULL(tabm.service_dealer_code, 0) AS service_dealer_code,
  0 AS customer_group,

  -- ������ ������������ (����� Order Type), ������
  TIMESTAMPDIFF(MONTH, COALESCE(tabm.last_invoice_date, tabm.last_sale_date, tabm.first_sale_date, tabm.vehicle_use_start_date), @BASE_DATE) AS idle_period,

  -- ������������ ������� �� ���� @BASE_DATE
  COALESCE(
    (tabm.mileage / IF(tabm.last_invoice_date = tabm.vehicle_use_start_date, NULL, TO_DAYS(tabm.last_invoice_date) - TO_DAYS(tabm.vehicle_use_start_date)) * (TO_DAYS(@BASE_DATE) - TO_DAYS(tabm.vehicle_use_start_date))),
    tabm.mileage,
    0
  ) AS mileage_predict

    FROM assp.bmw_tabm AS tabm
  
    WHERE TRUE 

      -- ������� ��������:
      AND tabm.service_dealer_code IN (32647)
      AND date(tabm.production_date) BETWEEN '2012-05-16' AND '2017-05-16'


--     HAVING TRUE
--       AND mileage_predict < 50000
;

-- �������� �������
SELECT * FROM assp.tmp_vin;


-- 
-- 2. �������������
-- ====================================================================================
CALL topic.ringlist_init();


-- 
-- 3. �������
-- ===================================================================================
  CALL topic.rf_oil();                      # �� ��������� ������ �����
  CALL topic.rf_remove_10_12_cylinders();   # 10-12 ���������
  CALL topic.rf_qlik();                     # ���������� ����������� � ���� BMW
  CALL topic.rf_persdata();                 # ������ � ��������� ����������� � ���� BMW

  CALL topic.rf_anonymous();                # ��������� ����� � Anonymous
  CALL topic.rf_out_of_assp();              # ����� ���������� ������ �� ��������� � ASSP
  CALL topic.rf_out_of_mini();              # ����� ���������� ������ �� ����� �������� ������� MINI

  CALL topic.rf_incalling();                # ������ ������ � ������� (�������� ����������)
  CALL topic.rf_silence(@SILENCE_DAYS);     # ������ � ������� ������
  CALL topic.rf_bmwstaff();                 # ���������� ������� BMW
  CALL topic.rf_nophone();                  # �� ������ ����� ��������
  CALL topic.rf_fleet_bmw();                # ������ (������ ���)
  CALL topic.rf_fleet(@ONLY_CYRILLIC);      # ��� ������� ���������� ��� ������
  CALL topic.rf_uncontact_bmw();            # ������������ (������ ���)
  CALL topic.rf_uncontact_clc();            # ������������ (������ ����-������)
  CALL topic.rf_notownbrand();              # �� ������� ������� (������ BMW)
  CALL topic.rf_notowncar();                # ����� ���� (������ ����-������)
  CALL topic.rf_wrongphone();               # ����� �� ����������� ������� (������ ����-������) 
  CALL topic.rf_unparsed_phone();           # ������������ �������
  CALL topic.rf_service_denial();           # ������ � ������ ������ (�� ������������� � ������)
  CALL topic.rf_ecode();                    # ������������ ECode
  CALL topic.rf_city();                     # ����� �� �������
  CALL topic.rf_region();                   # ������� ������ � ������� �� ���������
  CALL topic.rf_residence();                # ������ �� ��������� � ������� ������

-- ������� ������������:
-- ----------------------------------------

CALL topic.rf_min_idle(@MIN_IDLE_DAYS);   # ������� ���� ������� ����� ���������� ��������� �������
CALL topic.rf_max_idle(@MAX_IDLE_PERIOD); # ������� ����� ������� ����� ���������� ��������� �������


-- �������������� �������:
-- ----------------------------------------
CALL topic.rf_multiowner();               # ���������������
CALL topic.rf_phone_dbl();                # ������������� ��������


-- 
-- 4. �����
-- ===================================================================================
CALL topic.ringlist_log_stage(0, '', '�����');

-- �������� ������ ������� -- �� tmp_p (@WORK_TABLE)
DROP TEMPORARY TABLE IF EXISTS assp.tmp_list;
CREATE TEMPORARY TABLE assp.tmp_list AS 
SELECT 
  
  -- ���� ��� �� �������, �� ���������� firstname = '.'
  IFNULL(p.lastname, '') AS name_last, 
  IF(
    (TRIM(p.lastname) = '' OR  p.lastname IS NULL) AND (TRIM(p.firstname) = '' OR  p.firstname IS NULL) AND (TRIM(p.middlename) = '' OR  p.middlename IS NULL),
    '.', 
    p.firstname) AS name_first, 
  IFNULL(p.middlename, '') AS name_second, 
  #p.gender,
  p.crm_id,
  p.contact_id_crm,
  IF(p.brand = 'MOTO', 2, 1) AS vehicle_type,
  p.city_id, 

  -- ������������ ������� ���������� �������� '!'
  IF(p.tag_wrongphone & 2 > 0, CONCAT('!', p.phone2), p.clear_phone2) AS phone_business,
  IF(p.tag_wrongphone & 1 > 0, CONCAT('!', p.phone1), p.clear_phone1) AS phone_mobile,
  IF(p.tag_wrongphone & 4 > 0, CONCAT('!', p.phone3), p.clear_phone3) AS phone_home,
  p.email1 AS email1,
  p.email2 AS email2,
  p.contact_modified_on AS contact_modified,

  -- company_id - ������� �����
  p.company_id,   
  bdc.name AS service_dealer,

  -- ������ (��������)
--     p.customer_group,
  p.customer_group AS offer,
  IF(p.brand = 'MINI', 'MINI', 'BMW') AS ring_list_id,

  p.model AS model_name, 
  p.ecode AS body_code,
  p.production_year AS build_year,

  IF(p.brand = 'MINI', 25, 4) AS brand_id,

  p.vin AS vin,
  p.count_cylinders AS count_cylinders,
  tabm.basic_type AS cvp_type_code,
  IF(tabm.gear IS NULL, NULL, IF(UPPER(LEFT(tabm.gear, 1))='M', 'Manual', 'Automatic')) AS cvp_gearing,
  tabm.production_date AS cvp_production_date

  -- ������� ������ ������������
  # p.idle_period

  -- �������� ������ �������� ������ ��� ������������� -- ������ ����� ������� � clc_offer.
  -- �� ���� ������ ��������� �������� ���������������, ������������� ������������� �� clc_vehicle_ta.
  # a.action

FROM assp.tmp_p AS p
JOIN topic.bmw_dealer_code bdc ON bdc.caroperator_id = p.company_id 
LEFT JOIN assp.bmw_tabm tabm ON tabm.vin = p.vin

-- �������� (������������ �������� isofix �� ����������)
LEFT JOIN (
  SELECT vin, GROUP_CONCAT(ta_code ORDER BY ta_code) AS action
  FROM `ecm3-all`.clc_vehicle_ta 
  WHERE ta_code > 9999
    AND ta_code NOT IN (SELECT code FROM `ecm3-all`.clc_tech_action WHERE descr LIKE '%isofix%')
  GROUP BY vin
) AS a ON a.vin = p.vin

WHERE TRUE 
  AND p.disable = 0
  AND p.id > @LOAD_ID   

HAVING ring_list_id IS NOT NULL
;

SELECT * FROM assp.tmp_list;


-- 
-- 5. ������
-- ===================================================================================

-- REPORT  -- ������ �������� ������
CALL topic.ringlist_show_report();

-- VIEW    -- ������ ��� �������� ������� �� ������������ (������������ ������ ������)
CALL topic.ringlist_show_view(FALSE);

-- DETAILS -- ��������� ����� �� ����������� �� ��������� ������ -- �� tmp_p (@WORK_TABLE)
-- CALL topic.ringlist_show_details();
CALL topic.ringlist_show_details_simple();


-- 
-- 6. ������ � ���������� �������
-- ===================================================================================
CALL topic.ringlist_write_permanent(@WRITE_PERMANENT);
SET @LOG_TABLE      = NULL;
SET @RAW_TABLE      = NULL;
SET @WORK_TABLE     = NULL;
SET @CAMPAIGN_TABLE = NULL;
SET @CRMID          = NULL;
SET @BASE_DATE      = NULL;
